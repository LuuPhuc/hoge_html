/**
 * ページ下部の実際にはメインとなる情報の欄
 */
class AppendContent{
    constructor(_blockClass){
        this.blockClass = _blockClass;
        // 判定のエリアが入った時に発火したのか出ていった時に発火したのかを切り分ける
        //this.isDomBlockIn = false;
        //this.isDomSnapNextAreaIn = false;

        this.domBlock = document.querySelector(".append-content." + this.blockClass);

        // DOM イベント発火からの判別用
        this.domBlock.getObj = ()=> this;
    }
    /**
     * アニメーション等の開始処理
     */
    start(){
        console.log("start ", this.blockClass);
        setTimeout(() => {
        }, 300);
    }
    /**
     * 画面上の縦位置でどこに存在しているのかを調べる
     */
    getPosition(){
        return this.domBlock.getBoundingClientRect().top + window.pageYOffset;
    }
}