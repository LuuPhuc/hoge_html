/**
 * トップページ全体を管理する専用のクラス
 * トップページは非常に大量の状態をタイミングよく管理しないといけないので
 * それのための管理クラス。
 * タイミングのコントロールと中継ぎ状態管理はこのクラスが行う
 * 各クラスが極めて特定のコンテンツ用。汎用性はほぼ無視。
 * 
 */
class Top{
    constructor(){
        // 現在主に表示していると認識しているコンテンツ
        this.currentContent = "top-message";


        // 極めて特殊
        this.topMessage = new TopMessage();

        // 制御多し
        this.aboutContent = new Content("about");
        this.serviceContent = new Content("service");
        this.caseContent = new Content("case");

        // 普通
        this.clientsContent = new AppendContent("clients");
        this.partnershipContent = new AppendContent("partnership");
        this.newsContent = new AppendContent("news");
        this.recruitContent = new AppendContent("recruit");
        this.companyContent = new AppendContent("company");

        // 無視フラグ
        // イベントの重複によって振動したりガクつく場合があるので
        // ある程度無視するようにする
        this.ignore = false;
        
        this.domContentIndicator = {
            "top-message": {
                "label": document.querySelector("body > .current-location .labels .top-message"),
                "mark": document.querySelector("body > .current-location .marks .top-message")
            },
            "about": {
                "label": document.querySelector("body > .current-location .labels .about"),
                "mark": document.querySelector("body > .current-location .marks .about")
            },
            "service": {
                "label": document.querySelector("body > .current-location .labels .service"),
                "mark": document.querySelector("body > .current-location .marks .service")
            },
            "case": {
                "label": document.querySelector("body > .current-location .labels .case"),
                "mark": document.querySelector("body > .current-location .marks .case")
            },
            "clients": {
                "label": document.querySelector("body > .current-location .labels .clients"),
                "mark": document.querySelector("body > .current-location .marks .clients")
            },
            "partnership": {
                "label": document.querySelector("body > .current-location .labels .partnership"),
                "mark": document.querySelector("body > .current-location .marks .partnership")
            },
            "news": {
                "label": document.querySelector("body > .current-location .labels .news"),
                "mark": document.querySelector("body > .current-location .marks .news")
            },
            "recruit": {
                "label": document.querySelector("body > .current-location .labels .recruit"),
                "mark": document.querySelector("body > .current-location .marks .recruit")
            },
            "company": {
                "label": document.querySelector("body > .current-location .labels .company"),
                "mark": document.querySelector("body > .current-location .marks .company")
            }
        };


        /*
        var observer1 = new IntersectionObserver((entries, observer)=>{ 
            for(let i = 0; i < entries.length; i++){
                // console.log("retio", entries[i].target.getObj().blockClass, entries[i].intersectionRatio);
                if(entries[i].intersectionRatio < 0.6){
                    // 出ていく処理
                    // 入る処理
                    switch (entries[i].target.getObj().blockClass) {
                    case "about": this.outAbout(); break;
                    case "service": this.outService(); break;
                    case "case": this.outCase(); break;
                    default: break;
                    }
                }else{
                    // 入る処理
                    switch (entries[i].target.getObj().blockClass) {
                    case "about": this.goAbout(); break;
                    case "service": this.goService(); break;
                    case "case": this.goCase(); break;
                    case "clients": this.goClients(); break;
                    case "partnership": this.goPartnership(); break;
                    case "news": this.goNews(); break;
                    case "recruit": this.goRecruit(); break;
                    case "company": this.goCompany(); break;
                    default: break;
                    }
                }
            }
        }, { rootMargin: '20px 0px 0px 20px', threshold: 0.6 });

        observer1.observe(this.aboutContent.domBlock);
        observer1.observe(this.serviceContent.domBlock);
        observer1.observe(this.caseContent.domBlock);
        observer1.observe(this.clientsContent.domBlock);
        observer1.observe(this.partnershipContent.domBlock);
        observer1.observe(this.newsContent.domBlock);
        observer1.observe(this.recruitContent.domBlock);
        observer1.observe(this.companyContent.domBlock);
        */


        // 画面全体
        this.domBody = document.querySelector("body");
        // ヘッダーのロゴ
        this.domHeaderLogo = document.querySelector("header .logo");



        window.addEventListener("wheel", (e)=>{
            // ブラウザによって取れる値が違うのでノーマライズする
            var d = (e.deltaY < 0 || e.wheelDeltaY > 0) ? -1 : 1;
            var scrollTop = document.documentElement.scrollTop || document.body.scrollTop;

            //完全に about ブロックにあってさらに上に向かおうとする場合
            if(scrollTop <= 0 && d < 0){
                if(this.currentContent == "about"){
                    this.domBody.classList.add("lock");
                    this.topMessage.reset();
                    this.currentContent = "top-message";
                    e.preventDefault();  // 制御が入ったら実際のスクロールはさせない。
                    return;
                }
            }
            // 普通に下に向かう場合
            if(scrollTop >= 0 && d > 0){
                if(this.currentContent == "about"){
                    this.goService();
                    e.preventDefault();
                    return;
                }
                if(this.currentContent == "service"){
                    this.goCase();
                    e.preventDefault();
                    return;
                }
            }
            // 途中から上に向かう場合
            if(scrollTop >= 0 && d < 0){
                if(this.currentContent == "about"){
                }
                if(this.currentContent == "service"){
                    this.goAbout();
                    e.preventDefault();
                    return;
                }
                if(this.currentContent == "case"){
                    this.goService();
                    e.preventDefault();
                    return;
                }
            }
        });
        /*
        window.addEventListener("hashchange", (e)=>{
            console.log(location.hash);
            // ハッシュによるジャンプ動作を抑止する
            e.preventDefault();

        });
        */
    }
    /**
     * 開始
     */
    start(){
        this.goTopMessage();
    }
    /**
     * top-message へ遷移する
     */
    goTopMessage(){
        // 無視状態の場合はうけつけない
        if(this.ignore){ return }
        this.dull();

        // 初期化状態で構えさせてガクツキをなくす
        this.aboutContent.unload();

        this.topMessage.initStart();
        this.topMessage.start(()=>{
            // ここはパイ切り処理、終わった後の処理
            this.unlockScroll();
            this.goAboutFromTopMessage();
        });
        this.activateIndicator("top-message");
        this.currentContent = "top-message";
    }
    /**
     * about へ遷移する
     */
    goAbout(){
        if(this.ignore){ return }
        this.dull();

        console.log("goto about");

        // 制御として about から移動するのは top-message か service なので
        // ロックすることで細かいデフォルトのスクロール操作を発生させない
        // scrollTo はロック状態でも機能するのでコンテンツ遷移に問題は無い
        this.lockScroll();
        this.activateIndicator("about");
        var onScroll = ()=>{
            console.log("scrolled to about");
            window.removeEventListener("scroll", onScroll);
            this.currentContent = "about";
            this.aboutContent.start();
        };
        window.addEventListener("scroll", onScroll);
        window.scrollTo({
            top:  this.aboutContent.getPosition(),
            behavior: 'smooth',
        });
        //this.currentContent = "about";
        //this.aboutContent.start();
    }
    /**
     * top message から遷移してくる場合を想定している
     * スクロールしないので挙動が若干違う
     */
    goAboutFromTopMessage(){
        console.log("goto about from top message");
        this.activateIndicator("about");
        this.currentContent = "about";
        this.aboutContent.start();
        this.lockScroll();
    }
    outAbout(){
        this.aboutContent.unload();
    }
    /**
     * service へ遷移する
     */
    goService(){
        if(this.ignore){ return }
        this.dull(); 

        console.log("goto service", this.serviceContent.getPosition());
        this.activateIndicator("service");

        this.lockScroll();
        var onScroll = ()=>{
            console.log("scrolled to service");
            window.removeEventListener("scroll", onScroll);
            this.currentContent = "service";
            this.serviceContent.start();
        };
        window.addEventListener("scroll", onScroll);

        window.scrollTo({
            top:  this.serviceContent.getPosition(),
            behavior: 'smooth',
        });
    }
    outService(){
        this.serviceContent.unload();
    }
    /**
     * case へ遷移する
     */
    goCase(){
        if(this.ignore){ return }
        this.dull();

        this.lockScroll();
        console.log("goto case");
        this.activateIndicator("case");
        window.scrollTo({
            top:  this.caseContent.getPosition(),
            behavior: 'smooth',
        });
        this.currentContent = "case";
        this.caseContent.start();
    }
    outCase(){
        this.caseContent.unload();
    }

    goClients(){
        // ここからはスクロールを許すのでロック解除する
        this.unlockScroll();
        this.activateIndicator("clients");
        this.currentContent = "clients";
    }
    goPartnership(){
        this.activateIndicator("partnership");
        this.currentContent = "partnership";
    }
    goNews(){
        this.activateIndicator("news");
        this.currentContent = "news";
    }
    goRecruit(){
        this.activateIndicator("recruit");
        this.currentContent = "recruit";
    }
    goCompany(){
        this.activateIndicator("company");
        this.currentContent = "company";
    }

    /** 全体のスクロールを抑止する */
    lockScroll(){
        this.domBody.classList.add("lock");
    }
    /**
     * 全体のスクロールを開放する
     * CSS でスクロールバーを非表示にしているので、この動作による表示領域の増減は無い
     * */
    unlockScroll(){
        this.domBody.classList.remove("lock");
    }
    /**
     * イベントが高頻度で発生すると挙動がガクガクするので
     * ある程度ダルくする
     */
    dull(){
        this.ignore = true;
        setTimeout(()=>{
            this.ignore = false;
        }, 1000);
    }
    /**
     * 左端の表示を変える
     */
    activateIndicator(_name){
        console.log("remove class from ", this.currentContent);
        this.domContentIndicator[this.currentContent]["label"].classList.remove("current");
        this.domContentIndicator[this.currentContent]["mark"].classList.remove("current");

        console.log("add class to ", _name);
        this.domContentIndicator[_name]["label"].classList.add("current");
        this.domContentIndicator[_name]["mark"].classList.add("current");
    }
}