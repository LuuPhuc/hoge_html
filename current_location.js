class CurrentLocation{
    constructor(){
        this.current = "top-message";
        this.domBlock = document.querySelector("body > .current-location");
        this.domLabelList = document.querySelectorAll("body > .current-location ul.labels li");
        this.domMarkList = document.querySelectorAll("body > .current-location ul.marks li");

        this.doms = {
            "top-message": {
                "label": document.querySelector("body > .current-location .labels .top-message"),
                "mark": document.querySelector("body > .current-location .marks .top-message")
            },
            "about": {
                "label": document.querySelector("body > .current-location .labels .about"),
                "mark": document.querySelector("body > .current-location .marks .about")
            },
            "service": {
                "label": document.querySelector("body > .current-location .labels .service"),
                "mark": document.querySelector("body > .current-location .marks .service")
            },
            "case": {
                "label": document.querySelector("body > .current-location .labels .case"),
                "mark": document.querySelector("body > .current-location .marks .case")
            }
        };
    }
    activate(_name){
        this.doms[this.current]["label"].classList.remove("current");
        this.doms[this.current]["mark"].classList.remove("current");

        this.current = _name;

        this.doms[this.current]["label"].classList.add("current");
        this.doms[this.current]["mark"].classList.add("current");
    }
}