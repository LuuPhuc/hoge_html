class TopMessage{
    constructor(){
        
        // 自動処理中フラグ
        this.isAuto = true;

        // 全体動作時間 何ミリ秒で動作が完了するか？
        this.durationMSec = 1000;

        // 全体
        this.domBlock = document.querySelector("body > .top-message");

        // 次へ促すライン
        this.domNextLine = document.querySelector("body > .top-message .next-line");
        
        // テキストアニメーション用のオブジェクト
        this.svgImage = document.getElementById("top-message-text-animation");
        this.domLoader = document.querySelector("body > .top-message .loader");

        // パイ状に切り取るためのパスオブジェクト
        this.pieClipPath = document.querySelector("#top-message-svg defs clipPath polygon");

        this.wheelHandlerBinded = this.wheelHandler.bind(this);

        /*
        window.addEventListener("wheel", (e)=>{
            // 自動処理してるときはスクロール操作をうけつけない
            if(this.isAuto){
                return;
            }
            // ブラウザによって取れる値が違うのでノーマライズする
            var d = (e.deltaY < 0 || e.wheelDeltaY > 0) ? -1 : 1;
            //this.pieCutDrawStart(this.angle + (d * 90));
            //一気にオープンしてしまう仕様
            if(d == 1){
                this.pieCutDrawStart(90 + 361); // 条件に入れるために少しオーバーシュートする
            }
        });
        */

    }
    wheelHandler(e){
        console.log("wh");
        // 自動処理してるときはスクロール操作をうけつけない
        if(this.isAuto){
            return;
        }
        // ブラウザによって取れる値が違うのでノーマライズする
        var d = (e.deltaY < 0 || e.wheelDeltaY > 0) ? -1 : 1;
        //this.pieCutDrawStart(this.angle + (d * 90));
        //一気にオープンしてしまう仕様
        if(d == 1){
            this.pieCutDrawStart(90 + 361); // 条件に入れるために少しオーバーシュートする
        }
    }
    /**
     * 目標角を指定してパイカットアニメーション描画開始
     */
    pieCutDrawStart(_deg){
        console.log("pc");
        // 現在角度をスタート角度に
        this.startAngle = this.angle;
        // ゴール角度を指定角度へ
        this.goalAngle = _deg;
        // startTime 初期化
        this.startTime = null;

        this.pieCutDraw();
    }
    /**
     * パイ切り取りトランジションの描画メソッド
     * パラメータに1描画ご実行時刻が来る
     */
    pieCutDraw(_timestamp){
        // 最初の一発目は requestAnimationFrame からキックされないので timestamp が来ない
        // ので最初はキックだけして処理はしない
        if(!_timestamp){
            window.requestAnimationFrame((_t)=>{
                this.pieCutDraw(_t);
            });
            return;
        }
        // 初期処理
        if(this.startTime == null){
            this.startTime = _timestamp;
        }
        // 最初のからの経過時間
        var elapsedTime = _timestamp - this.startTime;
        // 経過時間の割合
        var elapsedTimeRate = elapsedTime / this.durationMSec;




        // 初期状態の方になったら四角に戻す
        if(this.angle < 90.05){
            this.isAuto = false;
            // 次のスタートに備えて内部の角度をちょっと進めておく
            this.angle = 91;
            // 四角に切り取る
            this.pieClipPath.setAttribute("points", "-200 -100   200 -100   200 100   -200 100");
            return;
        }

        // パイ切り取り処理
        // 座標は viewBox 基準 0 0 が画像中央部を指す
        // 上辺との交点に対する 
        var point1X = -100 / (Math.tan(this.angle / 180 * Math.PI));
        var point1Y = -100;
        // 右辺との交点に対する 
        var point2X = 200;
        var point2Y = 200 * (Math.tan(this.angle / 180 * Math.PI));

        // 下辺との交点に対する 
        var point3X = 100 / (Math.tan(this.angle / 180 * Math.PI));
        var point3Y = 100;
        
        // 左辺との交点に対する 
        var point4X = -200;
        var point4Y = -200 * (Math.tan(this.angle / 180 * Math.PI));

        //切り取りポイントが上辺右半分にある
        if(
            point1X >= 0 && point1X < 200 &&   // x のとりうる範囲
            point2Y <= point1Y &&              // 交点は手前と奥で2つあるので、どっちの交点を採用するのか
            this.angle >= 90 && this.angle < 270  // ダメ押し 角度縛り
            ){
            this.pieClipPath.setAttribute("points", "0 0   0 -100   -200 -100   -200 100   200 100   200 -100 " + point1X + " -100");
        }

        //切り取りポイントが右辺上半分にある
        if(
            point2Y >= -100 && point2Y <  0 &&
            point1X >= point2X &&
            this.angle >= 90 && this.angle < 270
            ){
            this.pieClipPath.setAttribute("points", "0 0   0 -100   -200 -100   -200 100   200 100   200 " + point2Y);
        }

        //切り取りポイントが右辺下半分にある
        if(
            point2Y >= 0 && point2Y < 100 &&
            point3X >= point2X &&
            this.angle >= 90 && this.angle < 270 
            ){
            this.pieClipPath.setAttribute("points", "0 0   0 -100   -200 -100   -200 100   200 100   200 " + point2Y);

        }

        //切り取りポイントが下辺右半分にある
        if(
            point3X <= 200 && point3X > 0 &&
            point2Y >= point3Y &&
            this.angle >= 90 && this.angle < 270
            ){
            this.pieClipPath.setAttribute("points", "0 0   0 -100   -200 -100   -200 100 " + point3X  + " 100");
        }

        //切り取りポイントが下辺左半分にある
        if(
            point3X <= 0 && point3X > -200 &&
            point4Y >= point3Y &&
            this.angle >= 270 && this.angle < 450
            ){
            this.pieClipPath.setAttribute("points", "0 0   0 -100   -200 -100   -200 100 " + point3X  + " 100");
        }

        //切り取りポイントが左辺下半分にある
        if(
            point4Y <= 100 && point4Y > 0 &&
            point3X <= point4X &&
            this.angle >= 270 && this.angle < 450
            ){
            this.pieClipPath.setAttribute("points", "0 0   0 -100   -200 -100   -200 " + point4Y);
        }

        //切り取りポイントが左辺上半分にある
        if(
            point4Y <= 0 && point4Y > -100 &&
            point1X <= point4X &&
            this.angle >= 270 && this.angle < 450
            ){
            this.pieClipPath.setAttribute("points", "0 0   0 -100   -200 -100   -200 " + point4Y);
        }
        //切り取りポイントが上辺左半分にある
        if(
            point1X > -200 && point1X < 0 &&
            point4Y <= point1Y &&
            this.angle >= 270 && this.angle < 450
            ){
            this.pieClipPath.setAttribute("points", "0 0   0 -100 " + point1X + " -100");
        }
        // ある角度まで来た場合に一気に最後まで行く
        /*
        if(this.angle > 260 && !this.isAuto){
            this.isAuto = true;
            // 現在角度をスタート角度に
            this.startAngle = this.angle;
            // ゴール角度を指定角度へ
            this.goalAngle = 451;
            // startTime 初期化
            this.startTime = null;
            this.durationMSec = 400;

            this.pieCutDraw();
        }
        */

        // この角度まできたら終了
        if(this.angle > 450){
            // プログラムの動作タイミングの関係で
            // このブロックは複数回実行されうる
            this.finish();
            return;
        }

        // 角度でアニメーション一旦終了
        // 許容値を小さくすると唐突感がやわらぐ
        if(Math.abs(this.angle - this.goalAngle) < 0.001){
            return;
        }


        // easeIn   (全体の変化幅) x (現在の変化割合の二乗) + 初期値
        // this.angle = (this.goalAngle - this.startAngle) * (elapsedTimeRate) + this.startAngle;
        // easeOut
        //this.angle = (this.goalAngle - this.startAngle) * (elapsedTimeRate * (2 - elapsedTimeRate)) + this.startAngle;
        
        this.angle = (this.goalAngle - this.startAngle) * (1 - Math.abs(Math.pow(elapsedTimeRate - 1, 2))) + this.startAngle;
        // InOut
        /*
        if(elapsedTimeRate < 0.5){
            this.angle = (this.goalAngle - this.startAngle) * (2 * elapsedTimeRate * elapsedTimeRate) + this.startAngle;
        }else{
            this.angle = (this.goalAngle - this.startAngle) * (-1 + (4 - 2 * elapsedTimeRate) * elapsedTimeRate) + this.startAngle;
        }
        */

        // 次の描画を予約する
        window.requestAnimationFrame((_t)=>{
            this.pieCutDraw(_t);
        });
    }
    // 開始初期状態にする
    // 最終状態になった際のコールバック
    initStart(_finishCallback){
        // 完了してない
        this.isFinish = false;
        // 自動処理中フラグ
        this.isAuto = true;

        // スタート角度(12時)
        // ピッタリで始めると表示がチラつくので1度から開始する
        this.startAngle = 91;
        // ゴールの角度
        this.goalAngle = this.startAngle + 359;
        // 現在角
        this.angle = this.startAngle;  // 初期は12時からスタートして時計回りに開いていく予定

        // アニメーションの経過時間を測る
        this.startTime = null;

    }

    /**
     * 描画、操作処理受付の開始
     * 最終状態に達した場合のコールバック
     */
    start(_finishCallback){
        // オブジェクトのメソッドは this が拘束されないので、拘束版をくっつけている
        window.addEventListener("wheel", this.wheelHandlerBinded); 
        //初期化処理されているモノとする
        // 処理を開始する
        this.loaderDrawStart(()=>{
            // ラインを引くアニメーションを引く
            this.nextLineDrawStart(()=>{
                // 引き終わったら自動制御を開放する
                this.isAuto = false;
            });
        });
        this.finishCallback = !_finishCallback ? ()=>{} : _finishCallback;
    }
    /**
     * 最終状態から初期状態まで戻るエフェクト
     */
    reset(){
        window.addEventListener("wheel", this.wheelHandlerBinded); 
        this.domBlock.classList.remove("finished");
        // 完了してない
        this.durationMSec = 1000;
        this.isFinish = false;
        // 自動処理中フラグ
        this.isAuto = true;
        // ピッタリで始めると表示がチラつくので1度から開始する
        this.goalAngle = 90;
        this.startAngle = this.goalAngle + 359;
        this.angle = this.startAngle;
        // アニメーションの経過時間を測る
        this.startTime = null;
        this.pieCutDrawStart(this.goalAngle);
    }

    /**
     * 仕様変更によりアニメーションしない。
     * テキストのタイプライターアニメーション描画開始
     * アニメーション終了時のコールバック
    textDrawStart(_finishCallback){
        // リロードした際にアニメーションが再生されないため余計なパラメータをあえてつけてロード
        this.svgImage.setAttributeNS('http://www.w3.org/1999/xlink', 'xlink:href', "images/top_message.gif?" + (new Date()).getTime());
        // 終了時がわからないので時間で適当に当てておく
        // タイプライターアニメーションの終わりを先読みで動作を入れておく
        setTimeout(()=>{
            _finishCallback();
        }, 4000);
    }
     */


    /**
     * コンテンツ描画中のチラツキを抑えるために最初数秒画面をガードする
     * @param {*} _finishCallback 
     */
    loaderDrawStart(_finishCallback){
        setTimeout(()=>{
            this.domLoader.classList.add("effect");
        }, 1000);
        // 1秒ガードして、アニメーションが1秒あるので、その後に発火するようにしてある
        setTimeout(()=>{
            _finishCallback();
        }, 2000);
    }

    /**
     * 次へ操作を促すラインを描画する
     */
    nextLineDrawStart(_finishCallback){
        this.domNextLine.classList.add("effect") ;
        setTimeout(()=>{
            _finishCallback();
        }, 1000);
    }
    /**
     * コンテンツ完了処理
     */
    finish(){
        // finish はタイミングによっては複数回連続で実行されるので
        // 抑止する。
        if(this.isFinish){
            return;
        }
        this.isFinish = true;
        this.domBlock.classList.add("finished");
        // オブジェクトのメソッドは this が拘束されないので、拘束版を引き剥がしている
        window.removeEventListener("wheel", this.wheelHandlerBinded);
        // 終了した際のコールバックをキックする
        this.finishCallback();
    }
}