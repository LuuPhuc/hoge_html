class Content{
    constructor(_blockClass){
        this.blockClass = _blockClass;
        // 判定のエリアが入った時に発火したのか出ていった時に発火したのかを切り分ける
        //this.isDomBlockIn = false;
        //this.isDomSnapNextAreaIn = false;

        this.domBlock = document.querySelector(".content." + this.blockClass);

        // DOM イベント発火からの判別用
        this.domBlock.getObj = ()=> this;
        this.domScrollMarker = document.querySelector(".content." + this.blockClass + " .scroll-marker");
        
        // 本文部分 
        this.domSection = document.querySelector(".content." + this.blockClass + " section"); 

        this.heading = document.querySelector(".content." + this.blockClass + " section h2"); 


        // 次へ促すライン
        this.domNextLine = document.querySelector(".content." + this.blockClass + " .next-line");
        // more のリンク
        this.domMore = document.querySelector(".content." + this.blockClass + " .more");

    }
    /**
     * アニメーション等の開始処理
     */
    start(){
        this.domSection.classList.add("effect");
        setTimeout(() => {
            this.domNextLine.classList.add("effect");
            this.domMore.classList.add("effect");
        }, 300);
    }
    // start と逆操作で画面から離れる時に呼び出される
    unload(){
        this.domSection.classList.remove("effect");
        this.domNextLine.classList.remove("effect");
        this.domMore.classList.remove("effect");
    }
    /**
     * more をクリックした際にどうなるか登録
     * @param {} _callback 
     */
    setClickMore(_callback){
        this.domMore.addEventListener("click", (e)=>{
            _callback();
            e.preventDefault();
        });
    }
    /**
     * 画面上の縦位置でどこに存在しているのかを調べる
     */
    getPosition(){
        return this.domBlock.getBoundingClientRect().top + window.pageYOffset;
    }
}