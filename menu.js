class Menu{
    constructor(){
        this.domMenu = document.querySelector("body > header > nav");
        this.domOpenButton = document.querySelector("body > header > .nav > button.menu-open");
        this.domOpenButton.addEventListener("click", ()=>{
            this.open();
        });
        // バブリング経由で閉じる
        this.domMenu.addEventListener("click", ()=>{
            this.close();
        });
    }

    open(){
        console.log("open");
        this.domMenu.classList.add("open");

    }
    close(){
        this.domMenu.classList.remove("open");
    }
}